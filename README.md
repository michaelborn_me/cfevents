# cfEvents

[![Bitbucket Pipelines build status](https://img.shields.io/bitbucket/pipelines/michaelborn_me/cfevents.svg?label=build&style=flat-square)](https://bitbucket.org/michaelborn_me/cfevents/addon/pipelines/home#!/results/branch/master/page/1) ![issues](https://img.shields.io/bitbucket/issues-raw/michaelborn_me/cfevents.svg?style=flat-square)

Make your native CFML app extendable by defining custom events, listening for those events, and running a closure when the event occurs. cfEvents is similar to the interceptors provided in Coldbox, only with less configuration required and fewer features. :)

## Getting Started

```bash
box install cfevents
```

Then include `cfevents` in some global scope.

```js
request.cfEvents = new cfevents.models.cfEvents();
```

## Listening to Events using Closures

Pass an event name and a closure in to the `listen()` method to listen for an event. The closure will get executed any time that `cfevents.run()` is called with that event name.

```js
request.cfEvents.listen("htmlHead", function() {
	// stick this file in the head
	writeOutput('<script src="/dist/app.js"></script>');
});
```

This closure will now get executed when `.run("htmlHead")` is called. (See "Running Events".)

```js
request.cfEvents.run("htmlHead");
```

## Listening to Events using Object Methods

A more organized way of using cfEvents might be to call an object method when that event triggers. To do this, pass the object as the third argument to `listen()`.

```js
var paypal = new paypal.paypal();
// "chargePaypal" function in paypal object will take the form parameters and send a payment charge.
request.cfEvents.listen( "formSubmit", "chargePaypal", paypal );
```

Here, `formSubmit` is the event name to listen for, and `chargePaypal` is the name of the function in the `paypal` object to execute when the event is triggered. This event can be triggered like any other event: using the `run()` function:

```js
request.cfEvents.run( "formSubmit", form);
```

Notice in this example we're passing the form scope to the event listener for easy form processing.

## Running Events

You can run an event by calling `cfEvents.run('eventName')`. This will execute **every** closure associated with that event **in order of creation**.

```js
request.cfEvents.run("htmlHead");
```

You can pass arguments to the executing closure by providing them as a struct. The arguments can be accessed from the `arguments` scope, just like any CFML function.

```js
var data = { "mailer": mailerObj, "data": form };
request.cfEvents.run("formSubmit", data);
```

## Event Listener Priority

If multiple listeners are defined for a given event name, the listeners will be executed by priority order - lowest numerical priority **first**, highest numerical priority **last**.

By default, event listeners are created with a priority of `10`.

```js
request.cfEvents.listen("formSubmit", myClosure);
```

Forcing a higher or lower priority will alter the listener execution order. We recommend staying in the 1-100 range.

```js
request.cfEvents.listen(name = "formSubmit", func = myClosure, priority = 5);
```

This would be useful for making sure that redirects happen before any buffer output via cfcontent or cfheader, for example.

## Variable Scoping in Events

First, remember that a closure will inherit the local scope of the defining block (e.g. `local` and `variables` scope _outside_ the closure definition are available _inside_ the closure).

Secondly, `this` inside an event closure will refer to the `cfEvents` class. So instead of calling component methods via `this.myFunc()`, use `variables.myFunc()` or just `myFunc()` if you don't care about scoping your vars. (I recommend scoping!)

## Engine Support

Sorry - this is currently Lucee only, due to the use of [parallel ArrayEach()](https://cfdocs.org/arrayeach).

* Lucee 4.5+

## TODO

* ✅ Execute events asynchronously
* ✅ Support component/method and closure-based listeners.
* ✅ Add WP-like priority execution - e.g. higher priority listeners get executed first.
* ❌ Add WP-like "filters" or similar to support return values.

## The Good News

> For all have sinned, and come short of the glory of God ([Romans 3:23](https://www.kingjamesbibleonline.org/Romans-3-23/))

> But God commendeth his love toward us, in that, while we were yet sinners, Christ died for us. ([Romans 5:8](https://www.kingjamesbibleonline.org/Romans-5-8))

> That if thou shalt confess with thy mouth the Lord Jesus, and shalt believe in thine heart that God hath raised him from the dead, thou shalt be saved. ([Romans 10:9](https://www.kingjamesbibleonline.org/Romans-10-9/))
 
## Repository

Copyright 2019 (and on) - [Michael Born](https://michaelborn.me/)

* [Homepage](https://bitbucket.org/michaelborn_me/cfevents/src/master/)
* [Issue Tracker](https://bitbucket.org/michaelborn_me/cfevents/issues?status=new&status=open)
* [New BSD License](https://bitbucket.org/michaelborn_me/cfevents/src/master/LICENSE.txt)

[![cfmlbadges](https://cfmlbadges.monkehworks.com/images/badges/made-with-cfml.svg)](https://cfmlbadges.monkehworks.com) [![cfmlbadges](https://cfmlbadges.monkehworks.com/images/badges/tested-with-testbox.svg)](https://cfmlbadges.monkehworks.com) [![cfmlbadges](https://cfmlbadges.monkehworks.com/images/badges/powered-by-coffee.svg)](https://cfmlbadges.monkehworks.com) [![cfmlbadges](https://cfmlbadges.monkehworks.com/images/badges/i-can-bench-press-ben-nadel.svg)](https://cfmlbadges.monkehworks.com)