/**
 * coldfusion wordpress-like events library
 */
component {
	property name="events" type="array";

	/**
	 * initializes the component and an empty event array
	 * allows code to run `new cfevents.cfevents()`.
	 */
	public component function init() {
			variables.events = [];
			return this;
	}

	/**
	 * I look for closures attached to the provided event name and run them. :)
	 * If multiple listeners are found for the event name, they will be executed in order of priority, where lowest numerical `priority` is the highest priority and thus executed first.
	 * 
	 * @name Give an event name to run. If no events are found, nothing happens.
	 * @args Pass argument data to the executing closure.
	 */
	public any function run( required string name, struct args = {} ) {
			var eventNameSearch = arguments.name;
			var argsToPass = arguments.args;
			// Execute only events matching the provided name
			var eventsToRun = variables.events.filter(function( event ) {
					return arguments.event.name == eventNameSearch;
			});
			// higher priority events should get executed first.
			eventsToRun.sort(function(event1, event2) {
				var firstHigherPriority = event1.priority < event2.priority;
				var equalPriority = event1.priority == event2.priority;
				return ( firstHigherPriority ? -1 : ( equalPriority ? 0 : 1 ) );
			});
			eventsToRun.each(function(event) {
				event.closure( argumentCollection = argsToPass );
			}, true);
	}

	/**
	 * I add a closure to the list of runnable events.
	 *
	 * @name event name to listen for
	 * @func If a user-defined function or closure, this function will be executed on event run and the `object` argument will be ignored. If a string, must be string name of an accessable function within the passed object. Any arguments from cfevents.run() will be passed to this function on execution. 
	 * @object reference to an instantiated object containing some method to run when executing the event. Ignored if `closure` is set to a valid closure.
	 */
	public void function listen( required string name, any func = "", component object, numeric priority = 10 ) {
		if ( isClosure(arguments.func) ) {
			variables.events.append( { name: arguments.name, closure: arguments.func, priority: arguments.priority } );
			return;
		}
		if ( arguments.keyExists("object")
			&& isValid("component", arguments.object)
			&& structKeyExists(arguments.object, arguments.func)
		) {
			var theClosure = function() { object[func]( argumentCollection = arguments ); };
			variables.events.append( { name: arguments.name, closure: theClosure, priority: arguments.priority } );
			return;
		}
		throw(
			message = "Invalid event listener",
			type="cfevents.invalidListener",
			detail="Must define a valid closure OR method name and component reference."
		);
	}

	/*
	 * I return all the runnable events and event metadata.
	 * INCLUDES duplicate event names, if `listen("myEvent",func)` is called multiple times.
	 */
	public array function getAll() {
			return variables.events;
	}

	/**
	 * I return an array of UNIQUE event names.
	 * No event metadata is returned, just an array of names.
	 */
	public array function getUniqueEvents() {
			var allEventNames = variables.events.map(function(item) {
					return item.name;
			});
			return listToArray( listRemoveDuplicates( ArrayToList( allEventNames ) ) );
	}
}