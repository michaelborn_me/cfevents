/**
* This tests the events functionality in cfEvents.cfc.
*/
component extends="testbox.system.BaseSpec"{

/*********************************** LIFE CYCLE Methods ***********************************/

	function beforeAll(){
		variables.mockbox = getMockBox();
	}

	function afterAll(){
		structDelete(application, "num");
		structDelete(variables, "mockbox");
	}

/*********************************** BDD SUITES ***********************************/

	function run(){

		/** 
		* describe() starts a suite group of spec tests.
		* Arguments:
		* @title The title of the suite, Usually how you want to name the desired behavior
		* @body A closure that will resemble the tests to execute.
		* @labels The list or array of labels this suite group belongs to
		* @asyncAll If you want to parallelize the execution of the defined specs in this suite group.
		* @skip A flag that tells TestBox to skip this suite group from testing if true
		*/
		describe( "cfEvents", function(){
		
			// before each spec in THIS suite group
			beforeEach(function(){
				variables.cfevents = new models.cfEvents();
			});
			
			// after each spec in THIS suite group
			afterEach(function(){
				structDelete(variables, "cfevents");
			});
			
			it("has zero events on init", function(){
				expect( variables.cfevents.getUniqueEvents() ).toBe( [] );
			});

			it("can add an event to listen to", function(){
				variables.cfevents.listen("html_render",function() {
					// do something
				});
				expect( variables.cfevents.getUniqueEvents() ).toBe( ["html_render"] );
			});
		
			it("can execute closure listeners", function(){
				application.num = 1;
				variables.cfevents.listen("html_render",function() {
					application.num = 2;
				});
				expect( application.num ).toBe( 1 );
				variables.cfevents.run("html_render");
				expect( application.num ).toBe( 2 );
			});

			it("can execute object-method listeners", function(){
				var testObject = mockbox.createStub();
				testObject.$(method="doCheckoutCalc", callback = function() {
					application.num = 2;
				});
				application.num = 1;
				variables.cfevents.listen("postCheckout", "doCheckoutCalc", testObject);
				expect( application.num ).toBe( 1 );
				variables.cfevents.run("postCheckout");
				expect( application.num ).toBe( 2 );
			});
		
			it("can pass arguments to the executing event", function(){
				variables.cfevents.listen("myEvent",function() {
					expect( arguments ).toHaveKey("test");
					expect( arguments.test ).toBe( "123" );
				});
				variables.cfevents.run("myEvent", { test: "123"});
			});
		
			it("runs events asynchronously", function(){
				variables.cfevents.listen("myEvent",function() {
					sleep(100);
				});
				variables.cfevents.listen("myEvent",function() {
					sleep(100);
				});
				variables.cfevents.listen("myEvent",function() {
					sleep(100);
				});
				variables.cfevents.listen("myEvent",function() {
					sleep(100);
				});
				var start = getTickCount();
				variables.cfevents.run("myEvent", { test: "123"});
				var end = getTickCount();
				// if cfevents can execute four 100-millisecond events in less than 200 ms, 
				// then something called "async" must be going on. :)
				expect( end - start ).toBeLT( 200 );
			});

			it("will throw error if function not found in object", function(){
				var fakeObject = mockbox.createStub();
				expect( function() {
					variables.cfevents.listen("postCheckout", "nonExistingMethod", fakeObject);
				} ).toThrow( "cfevents.invalidListener" );
			});

			it("will throw error if `function` arg not a function and `object` not passed", function(){
				expect( function() {
					variables.cfevents.listen("postCheckout", "notAFunction");
				} ).toThrow( "cfevents.invalidListener" );
			});

			it("will execute higher-priority listeners first", function(){
				application.num = 1;

				// if the listener defined first runs first, application.num will be 18. ((1 * 20)-2)
				// if the listener with the highest priority runs first, application.num will be -1. ((1 -2)*20)
				variables.cfevents.listen("doSomething", function() {application.num = application.num * 20} );

				// explicitly define a high priority
				variables.cfevents.listen(
					name = "doSomething",
					func = function() { application.num = application.num - 2 },
					priority = 1
				);

				variables.cfevents.run("doSomething");
				expect( application.num ).toBe( -20 );
			});
		});


	}
}